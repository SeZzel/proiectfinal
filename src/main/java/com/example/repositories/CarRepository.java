package com.example.repositories;

import com.example.model.Car;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository  extends CrudRepository<Car,Long>{

Car findByBrand(String Brand);
Car findByChassisCode(String ChassisCode);
void deleteById(Long id);
}
