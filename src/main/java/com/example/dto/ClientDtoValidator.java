package com.example.dto;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


public class ClientDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }

    @Override
    public void validate(Object target, Errors errors) {
        ClientDto ClientDto = (ClientDto) target;

        ValidationUtils.rejectIfEmpty(errors, "author.firstName", "Not Empty");
        if (ClientDto.getClient().getFirstName().length() < 3 || ClientDto.getClient().getFirstName().length() > 32) ;
        errors.rejectValue("client.firstName", "size.firstName");


        ValidationUtils.rejectIfEmpty(errors, "author.lastName", "Not Empty");
        if (ClientDto.getClient().getLastName().length() < 3 || ClientDto.getClient().getLastName().length() > 32) ;
        errors.rejectValue("client.lastName", "size.lastName");

        ValidationUtils.rejectIfEmpty(errors, "author.lastName", "Not Empty");
        if (ClientDto.getClient().getAge() < 3 || ClientDto.getClient().getAge() >90 ) ;
        errors.rejectValue("client.age", "age");

        ValidationUtils.rejectIfEmpty(errors, "author.lastName", "Not Empty");
        if (ClientDto.getClient().getPhoneNumber().length() < 10 || ClientDto.getClient().getPhoneNumber().length() > 32) ;
        errors.rejectValue("client.phoneNumber", "size.phoneNumber");

}
}
