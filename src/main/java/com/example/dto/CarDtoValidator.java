package com.example.dto;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class CarDtoValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return false;
    }
    @Override
    public void validate(Object target, Errors errors) {
        CarDto CarDto = (CarDto) target;

        ValidationUtils.rejectIfEmpty(errors, "car.brand", "Not Empty");
        if (CarDto.getCar().getBrand().length() < 3 || CarDto.getCar().getBrand().length() > 32) ;
        errors.rejectValue("car.brand", "size.brand");


        ValidationUtils.rejectIfEmpty(errors, "car. name", "Not Empty");
        if (CarDto.getCar().getName().length() < 1 || CarDto.getCar().getName().length() > 32) ;
        errors.rejectValue("car.name", "size.name");

        ValidationUtils.rejectIfEmpty(errors, "car.chassisCode", "Not Empty");
        if (CarDto.getCar().getChassisCode().length() < 9 || CarDto.getCar().getChassisCode().length() > 32) ;
        errors.rejectValue("car.chassisCode", "size.chassisCode");

        ValidationUtils.rejectIfEmpty(errors, "car.mileage", "Not Empty");
        if (CarDto.getCar().getMilage()< 1 || CarDto.getCar().getMilage() > 10000000) ;
        errors.rejectValue("car.mileage", "mileage");

    }

}
