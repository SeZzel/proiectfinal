package com.example.dto;

import com.example.model.Car;
import com.example.model.Client;

public class ClientDto {
    private Client client;
    private Car car;

    public ClientDto() {
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}
