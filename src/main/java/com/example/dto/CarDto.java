package com.example.dto;

import com.example.model.Car;
import com.example.model.Client;

public class CarDto {
    private Car car;

    private Client client;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public CarDto() {
    }
}

