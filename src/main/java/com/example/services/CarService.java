
package com.example.services;

import com.example.model.Car;
import com.example.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CarService {
    public CarRepository carRepository;

    @Autowired

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getCarJson() {
        List<Car> carList = new ArrayList<>();
        carRepository.findAll().forEach(carList::add);
        carRepository.findAll().forEach(carFromDb -> carList.add(carFromDb));
        return carList;
    }

    //    public Car getCar(@PathVariable Long id ){
//        return carRepository.findAllById(id).orElseTrow(()-> new CarNotFoundException(id));
//
//    }
    public void deleteAuthorsJson() {
        carRepository.deleteAll();
    }

    public void delete(Long id) {
        carRepository.deleteById(id);
    }

    public Car findCarById(Long id) {
        return carRepository.findById(id).get();
    }

    public Car saveAuthorJson(Car car) {
        return carRepository.save(car);
    }
}
