package com.example.services;

import com.example.model.Client;
import com.example.repositories.ClientRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientService {
    public ClientRepository clientRepository;

    public ClientService( ClientRepository clientRepository){this.clientRepository=clientRepository;}
public List<Client>getClientJson(){
        List<Client>clientList=new ArrayList<>();
        clientRepository.findAll().forEach(clientList::add);
        return clientList;
}
}
