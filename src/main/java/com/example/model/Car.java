package com.example.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
public class Car {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @Column (name="brand")
    private String brand;
    @Column(name ="name")
    private String name;
    @Column(name= "chasssisCode")
    private String chassisCode;
    @Column (name="description")
    private String description;
    @Column (name= "buildYear")
    private int buildYear;
    @Column (name="hp")
    private int hp;
    @Column(name="mileage")
    private int mileage;


    @ManyToMany(cascade=CascadeType.PERSIST,mappedBy = "clientList",fetch=FetchType.EAGER)
    private Set<Car> carList=new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(id, car.id);
    }

    public Car() {
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", name='" + name + '\'' +
                ",chassisCode" + chassisCode + '\'' +
                ", description='" + description + '\'' +
                ", buildYear=" + buildYear +
                ", hp=" + hp +
                ", mile`age=" + mileage +
                ", carList=" + carList +
                '}';
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChassisCode() {
        return chassisCode;
    }

    public void setChassisCode(String chassisCode) {
        this.chassisCode = chassisCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBuildYear() {
        return buildYear;
    }

    public void setBuildYear(int buildYear) {
        this.buildYear = buildYear;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getMilage() {
        return mileage;
    }

    public void setMilage(int milage) {
        this.mileage = milage;
    }

    public Set<Car> getCarList() {
        return carList;
    }

    public void setCarList(Set<Car> carList) {
        this.carList = carList;
    }
}
