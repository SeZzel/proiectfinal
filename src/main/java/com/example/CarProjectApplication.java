package com.example;

import com.example.repositories.CarRepository;
import com.example.repositories.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarProjectApplication implements CommandLineRunner {

		@Autowired
				private ClientRepository clientRepository;
		@Autowired
				private CarRepository carRepository;
		public static void main(String[] args) { SpringApplication.run(CarProjectApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


	}
}
