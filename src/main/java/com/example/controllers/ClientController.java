package com.example.controllers;

import com.example.model.Car;
import com.example.model.Client;
import com.example.repositories.CarRepository;
import com.example.repositories.ClientRepository;
import com.example.services.CarService;
import com.example.services.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@Controller
public class ClientController {
    @Autowired
    public ClientRepository clientRepository;
    @Autowired
    public ClientService clientService;
    public ClientController(ClientService carService){this.clientService=clientService;}
    @RequestMapping("/security")
    public String getSecutiry() {
        return "login";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/client")
    public String getClient(Model model) {

        model.addAttribute("clientList", clientRepository.findAll());
        return "client";

    }



//    @RequestMapping("/edit/{id}")
//
//    public String edit(@PathVariable Long id,Model model) {
//        clientService.findClientById(id);
//        model.addAttribute("client", clientService.findClientById(id));
//        return "editClient";
//    }

}

