package com.example.controllers;

public class ClientNotFoundException extends RuntimeException{
    public ClientNotFoundException(long id ){
        super("Could not find client with this id"+id);
    }
}
