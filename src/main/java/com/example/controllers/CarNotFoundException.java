package com.example.controllers;

public class CarNotFoundException extends RuntimeException {
    public CarNotFoundException(long id ){
        super("Could not find Car with this id"+id);
    }
}
