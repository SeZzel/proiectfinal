package com.example.controllers;

import com.example.model.Car;
import com.example.repositories.CarRepository;
import com.example.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class CarController {
@Autowired
    public CarRepository carRepository;
@Autowired
    public CarService carService;
public CarController(CarService carService){this.carService=carService;}
    @RequestMapping("/security")
    public String getSecutiry() {
        return "login";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/car")
    public String getCar(Model model) {

        model.addAttribute("cars", carRepository.findAll());
        return "car";

    }

    @GetMapping("/rest/car")
    public @ResponseBody
    List<Car> getCarsJson() {

        return carService.getCarJson();
    }

//    @GetMapping("rest/getCarById")
//    public @ResponseBody
//   Car getAuthorById(@PathVariable Long id) {
//        return carService.getCar(id);
//    }

    @DeleteMapping("/rest/delete-authors")
    public @ResponseBody
    void deleteAuthorsJson() {
        carService.deleteAuthorsJson();
    }

    @RequestMapping("/delete/{id}")
    public String

    delete(@PathVariable Long id) {
        carService.delete(id);
        return "redirect:/author";
    }

    @RequestMapping("/edit/{id}")

    public String edit(@PathVariable Long id,Model model) {
       carService.findCarById(id);
        model.addAttribute("car", carService.findCarById(id));
        return "editCar";
    }

    }
